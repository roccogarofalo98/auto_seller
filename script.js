fetch('./data.json').then(response => response.json()).then(data => {
    
    // COLONNA TUTTE LE MARCHE DI AUTO
    function allCarMake(datiInIngresso) {
        let carMake = Array.from(new Set(datiInIngresso.map(el => el.car_make))).sort()
        let carWrapper = document.querySelector('#carWrapper')
        carMake.forEach(el => {
            
            
            let div = document.createElement('div')
            div.classList.add('custom-control', 'custom-radio')
            div.innerHTML = 
            `
            <input type="radio" id="${el}" name="Mark" class="custom-control-input">
            <label class="custom-control-label text-seco" for="${el}">${el}</label>
            `
            
            carWrapper.appendChild(div)
            
        })
        
        let allMark = document.createElement('div')
        allMark.classList.add('custom-control', 'custom-radio')
        allMark.innerHTML = 
        `
        <input type="radio" id="allMark" name="Mark" class="custom-control-input">
        <label class="custom-control-label text-seco" for="allMark">All Mark</label>
        `
        
        carWrapper.appendChild(allMark)
    }
    allCarMake(data)

    // CARD TUTTE LE AUTO
    function allProducts(datiIngresso) {
        
        let productsWrapper = document.querySelector('#productsWrapper')
        productsWrapper.innerHTML= ""
        
        // let sorted = datiIngresso.sort((a,b) => (a.car_make).localeCompare(b.car_make))
        
        datiIngresso.sort((a,b) => (a.car_make.localeCompare(b.car_make))).forEach(el => {
            
            let div = document.createElement('div')
            div.classList.add('ho')
            div.innerHTML = 
            `
            <div class="card-custom mb-3" style="max-width: 100%; max-height: 100%;">
            <div class="row no-gutters">
            <div class="col-md-4">
            <img src="https://picsum.photos/250/250/" class="card-img" alt="img-car">
            </div>
            <div class="col-md-8">
            <div class="card-body">
            <h4 class="card-title text-seco">${el.car_make} ${el.car_model}</h4>
            <p class="card-text"><small class="text-seco">${el.car_model_year}</small></p>
            <p class="card-text"><small class="text-seco">${el.street_adress}, ${el.city}, ${el.country}</small></p>
            <p class="card_text text-right text-main h4">$${el.price}</p>
            </div>
            </div>
            </div>
            </div>
            `
            
            productsWrapper.appendChild(div)
            
        })
    }
    allProducts(data)

    // FILTRO PER MARCHE AUTO  
    function manageFilters() {
        let filtered = document.querySelectorAll('input.custom-control-input')

        filtered.forEach(el => {
            el.addEventListener( 'click', ()=> {

                let checkMake = el.id

                if (checkMake != "allMark") {
                    let productsFilter = data.filter(el => el.car_make == checkMake)
                    allProducts(productsFilter);
                } else {
                    allProducts(data);   
                }

            })
        })
    }
    manageFilters()

    // FILTRO PER PREZZO
    function mangePrice(params) {
    // MAX PREZZO
    // facendo il metodo commentato usiamo lo spred operator cioe Math.max(...) cosi facendo trasformeremo un array in una lista
    // let maxPriceFromData = Math.max(...data.map(el => Number(el.price)))
    let maxPriceFromData = Array.from(data.map(el => Number(el.price))).sort((a,b)=> b-a)[0]
    document.querySelector('#maxPrice').innerText = `$${maxPriceFromData}`

    
    let sliderValue = document.querySelector('#sliderValue')
    sliderValue.innerText = `$${maxPriceFromData}`
    
    // SLIDER PREZZO
    
    let slider = document.querySelector('#slider')
    slider.max = Math.ceil(maxPriceFromData)
    slider.value = Math.ceil(maxPriceFromData)
    
    slider.addEventListener('input', ()=>{
        let sliderAllowed = slider.value;

        sliderValue.innerText = `$${sliderAllowed}`

        let filteredProducts = data.filter(el => Number(el.price) <= slider.value )
        allProducts(filteredProducts);
    })
    }
    mangePrice()

    // FILTRO PER PAROLA
    function filetrWord() {
        let searchedWord = document.querySelector('#searchedWord')
        searchedWord.addEventListener('input', ()=>{
            
        let filteredProducts = data.filter(el => el.car_model.toLowerCase().includes(searchedWord.value.toLowerCase()))
        allProducts(filteredProducts);
        
        // let filteredProductsMark = data.filter(el => el.car_make.toLowerCase().includes(searchedWord.value.toLowerCase()))
        // allProducts(filteredProductsMark);
    
        })
    }
    filetrWord()

    // FAR GIRARE COSE  
    function girareRobe() {
        document.addEventListener('scroll', ()=>{
            let spettacolo = document.querySelector('#spettacolo')
            let scrolled = window.pageYOffset

            spettacolo.style.transform = `rotate(${scrolled}deg)`
        })
    }
    girareRobe()
    
})